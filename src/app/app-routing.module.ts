import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthFormComponent } from './auth/auth-form/auth-form.component';
import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
  {
    path: 'categories',
    canActivate: [AuthGuard],
    loadChildren: () => import('./category/category.module').then(m => m.CategoryModule),
  },
  {
    path: 'categories/:category/products',
    canActivate: [AuthGuard],
    loadChildren: () => import('./product/product.module').then(m => m.ProductModule),
  },
  {
    path: 'products',
    canActivate: [AuthGuard],
    loadChildren: () => import('./product/product.module').then(m => m.ProductModule),
  },
  {
    path: 'auth',
    component: AuthFormComponent
  },
  { path: '**', redirectTo: 'categories' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
