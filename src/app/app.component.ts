import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AuthService } from './auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
  public isLogged$ = this.authService.isLogged;

  constructor(
    private readonly authService: AuthService,
  ) {
  }

  public logout(): void {
    this.authService.logout();
  }
}
