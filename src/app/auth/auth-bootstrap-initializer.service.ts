import { APP_INITIALIZER, FactoryProvider } from '@angular/core';

import { AuthBootstrapService } from './auth-bootstrap.service';

export const authBootstrap =
  (bootstrap: AuthBootstrapService) => () => bootstrap.persistAuth().toPromise();

export const authBootstrapInitializer: FactoryProvider = {
  provide: APP_INITIALIZER,
  useFactory: authBootstrap,
  deps: [AuthBootstrapService],
  multi: true,
};
