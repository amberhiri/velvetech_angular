import { Injectable } from '@angular/core';
import { AUTH_TOKEN_KEY, AuthService } from './auth.service';
import { TokenService } from './token.service';
import { EMPTY, Observable } from 'rxjs';

@Injectable()
export class AuthBootstrapService {
  constructor(
    private readonly authService: AuthService,
    private readonly tokenService: TokenService,
  ) {
  }

  public persistAuth(): Observable<void> {
    const token = this.tokenService.getToken(AUTH_TOKEN_KEY);

    if (token) {
      this.authService.login();
    }

    return EMPTY;
  }
}
