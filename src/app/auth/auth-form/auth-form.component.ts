import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-auth-form',
  templateUrl: './auth-form.component.html',
  styleUrls: ['./auth-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuthFormComponent {
  public authForm = new FormGroup({
    login: new FormControl(null,  Validators.required),
    password: new FormControl(null,  Validators.required),
  });

  public isSubmitDisabled$ = this.authForm.statusChanges
    .pipe(
      map(status => status === 'INVALID'),
    );

  constructor(
    private readonly authService: AuthService,
  ) { }

  public login(): void {
    const creds = this.authForm.value;

    this.authService.login(creds);
  }
}
