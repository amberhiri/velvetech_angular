import { Injectable } from '@angular/core';
import { CanActivate, Router, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { tap } from 'rxjs/operators';

type CanActivateReturnType = Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree;
type CanLoadReturnType = CanActivateReturnType;

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private readonly router: Router,
    private readonly authService: AuthService,
  ) {
  }

  public canActivate(route?, state?): CanActivateReturnType {
    return this.authService.isLogged
      .pipe(
        tap((value: boolean) => {
          if (!value) {
            this.router.navigate(
              [`/auth`],
              {
                queryParams: {redirectTo: state.url},
                state: {params: route.queryParams}
              }
            );
          }
        }),
      );
  }

  public canLoad(): CanLoadReturnType {
    return this.canActivate();
  }
}
