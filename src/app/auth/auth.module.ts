import { InjectionToken, ModuleWithProviders, NgModule } from '@angular/core';
import { AuthFormComponent } from './auth-form/auth-form.component';
import { AuthGuard } from './auth.guard';
import { AuthService } from './auth.service';
import { TokenService } from './token.service';
import { AuthBootstrapService } from './auth-bootstrap.service';
import { authBootstrapInitializer } from './auth-bootstrap-initializer.service';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

export function provideForRootGuard(authModule: AuthModule): string {
  if (authModule) {
    throw new Error('AuthModule.forRoot() called twice.');
  }
  return 'guarded';
}

export const AUTH_ROOT_GUARD = new InjectionToken<AuthModule>('AUTH_ROOT_GUARD');

@NgModule({
  declarations: [
    AuthFormComponent,
  ],
  exports: [
    AuthFormComponent,
  ],
  imports: [
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    CommonModule,
  ]
})
export class AuthModule {
  public static forRoot(): ModuleWithProviders<AuthModule> {
    return {
      ngModule: AuthModule,
      providers: [
        AuthGuard,
        AuthService,
        TokenService,
        AuthBootstrapService,
        authBootstrapInitializer,
        {
          provide: AUTH_ROOT_GUARD,
          useFactory: provideForRootGuard,
          deps: [AuthModule]
        }
      ]
    };
  }
}

