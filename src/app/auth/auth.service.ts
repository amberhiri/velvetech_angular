import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { TokenService } from './token.service';
import { Router } from '@angular/router';

export interface Credentials {
  login: string;
  password: string;
}

export const AUTH_TOKEN_KEY = 'auth-token';

@Injectable()
export class AuthService {
  private isLogged$ = new BehaviorSubject(false);

  constructor(
    private readonly router: Router,
    private readonly tokenService: TokenService,
  ) {
  }

  public get isLogged(): Observable<boolean> {
    return this.isLogged$.asObservable();
  }

  public login(creds?: Credentials): void {
    this.tokenService.setToken(AUTH_TOKEN_KEY, 'super-secure-auth-token');
    this.isLogged$.next(true);
    this.router.navigateByUrl('/');
  }

  public logout(): void {
    this.tokenService.destroyToken(AUTH_TOKEN_KEY);
    this.isLogged$.next(false);
    this.router.navigateByUrl('/auth');
  }
}
