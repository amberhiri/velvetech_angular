import { Injectable } from '@angular/core';

@Injectable()
export class TokenService {
  public getToken(key: string): string {
    const token = localStorage.getItem(key);

    return token || '';
  }

  public setToken(key: string, token: string): void {
    if (token) {
      localStorage.setItem(key, token);
    }
  }

  public destroyToken(key: string): void {
    localStorage.removeItem(key);
  }
}
