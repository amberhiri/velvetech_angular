import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export type CreateEntityFn<TEntity> = (payload?: Partial<TEntity>) => TEntity;

export class BaseEntityService<TEntity> {
  private readonly entityList$: BehaviorSubject<Array<TEntity>>;
  private readonly idKey: string;
  private readonly createEntityFn: CreateEntityFn<TEntity>;

  constructor(
    payload: Array<TEntity>,
    idKey: string,
    createEntityFn: CreateEntityFn<TEntity>,
  ) {
    this.entityList$ = new BehaviorSubject<Array<TEntity>>(payload);
    this.idKey = idKey;
    this.createEntityFn = createEntityFn;
  }

  public getEntity(byId: string): Observable<TEntity | null> {
    return this.entityList$.pipe(
      map(list => list.find(entity => entity[this.idKey] === byId) || null)
    );
  }

  public get entityList(): Observable<Array<TEntity>> {
    return this.entityList$.asObservable();
  }

  public add(payload: TEntity): void {
    const entity = this.createEntityFn(payload);
    const list = [entity, ...this.entityList$.value];

    this.entityList$.next(list);
  }

  public update(payload: Partial<TEntity>): void {
    const mappedList = this.entityList$.value?.map(
      entity => entity[this.idKey] === payload[this.idKey] ? {...entity, ...payload } : entity,
    );

    this.entityList$.next(mappedList);
  }

  public updateAll(payload: Partial<TEntity>, predicate?: (entity: TEntity) => boolean): void {
    const mappedList = this.entityList$.value?.map(entity => {
      if (predicate) {
        return predicate(entity) ? {...entity, ...payload } : entity;
      } else {
        return {...entity, ...payload };
      }
    });

    this.entityList$.next(mappedList);
  }

  public remove(byId: string): void {
    const filteredList = this.entityList$.value?.filter(entity => entity[this.idKey] !== byId);

    this.entityList$.next(filteredList);
  }
}
