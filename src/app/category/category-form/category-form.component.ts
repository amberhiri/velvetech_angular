import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { map, shareReplay, startWith, take, takeUntil } from 'rxjs/operators';
import { Category } from '../category';
import { CategoryService } from '../category.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoryFormComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();

  public categoryForm = new FormGroup({
    title: new FormControl(null,  [Validators.required, Validators.minLength(5), Validators.maxLength(40)]),
  });

  public isSubmitDisabled$ = this.categoryForm.statusChanges
    .pipe(
      startWith(this.categoryForm.status),
      map(status => status === 'INVALID'),
    );

  public strategy: 'edit' | 'add' = 'add';
  public categoryId: string;
  public category$: Observable<Category>;

  constructor(
    private readonly categoryService: CategoryService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
  ) {
  }

  ngOnInit(): void {
    this.strategy = this.activatedRoute.snapshot.data.strategy;
    this.categoryId = this.activatedRoute.snapshot.params.category;

    if (this.strategy === 'edit' && this.categoryId) {
      this.initEditStrategy();
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }

  public save(): void {
    if (this.strategy === 'edit') {
      this.categoryService.update({
        ...this.categoryForm.value,
        id: this.categoryId,
      });
    } else {
      this.categoryService.add(this.categoryForm.value);
    }

    this.router.navigate(['../']);
  }

  private initEditStrategy(): void {
    this.category$ = this.categoryService.getEntity(this.categoryId).pipe(shareReplay(1));

    this.category$
      .pipe(
        take(1),
        takeUntil(this.destroy$),
      )
      .subscribe(
        category => {
          const { id, ...formPrefill } = category;

          this.categoryForm.patchValue(formPrefill);
        }
      );
  }
}
