import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { CategoryService } from '../category.service';
import { Category } from '../category';
import { take, takeUntil, withLatestFrom } from 'rxjs/operators';
import { ProductService } from '../../product/product.service';
import { categoryList } from '../category.mock';
import { getRandomIntInclusive } from '../../product/product.mock';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoryListComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  public displayedColumns: string[] = ['title', 'actions'];
  public dataSource$ = this.categoryService.entityList;

  constructor(
    private readonly categoryService: CategoryService,
    private readonly productService: ProductService,
  ) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }

  public removeCategory(byId: string): void {
    this.categoryService.entityList
      .pipe(
        take(1),
        takeUntil(this.destroy$),
      )
      .subscribe(
        categories => {
          const safeCategories = categories.filter(category => category.id !== byId);
          const randomCategory = safeCategories[getRandomIntInclusive(1, safeCategories.length - 1)];

          this.productService.updateAll({ category: randomCategory.id }, product => product.category === byId);
          this.categoryService.remove(byId);
        }
      );
  }
}
