import { Category } from './category';
import * as faker from 'faker';
import { CreateEntityFn } from '../base-crud-service';

export const createCategory: CreateEntityFn<Category> = (payload?: Partial<Category>): Category => {
  return {
    title: payload?.title || faker.commerce.department(),
    id: faker.random.alphaNumeric(43),
  };
};

export const categoryList: Array<Category> = new Array(10)
  .fill(1)
  .map(() => createCategory());
