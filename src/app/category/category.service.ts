import { Injectable } from '@angular/core';
import { categoryList, createCategory } from './category.mock';
import { Category } from './category';
import { BaseEntityService } from '../base-crud-service';

@Injectable({providedIn: 'root'})
export class CategoryService extends BaseEntityService<Category> {
  constructor() {
    super(categoryList, 'id', createCategory);
  }
}
