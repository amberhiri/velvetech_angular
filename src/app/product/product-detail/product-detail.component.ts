import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { ProductService } from '../product.service';
import { Observable } from 'rxjs';
import { CategoryService } from '../../category/category.service';
import { map, withLatestFrom } from 'rxjs/operators';
import { ProductView } from '../product-list/product-list.component';
import { Category } from '../../category/category';
import { Product } from '../product';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductDetailComponent implements OnInit {

  public product$: Observable<ProductView> = this.productService
    .getEntity(this.activatedRoute.snapshot.params.product)
    .pipe(
      withLatestFrom(this.categoryService.entityList),
      map(([product, categoryList]) => ({
        ...product,
        category: categoryList.find(category => category.id === product.category),
      }))
    );

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly productService: ProductService,
    private readonly categoryService: CategoryService,
    private readonly router: Router,
  ) {
  }

  ngOnInit(): void {
  }

  public edit(product: ProductView): void {
    const category = this.activatedRoute.snapshot.params.category;

    if (category) {
      this.router.navigate(['/categories', category, 'products', 'edit', product.id]);
    } else {
      this.router.navigate(['/products/edit', product.id]);
    }
  }
}
