import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { map, shareReplay, startWith, take, takeUntil } from 'rxjs/operators';
import { ProductService } from '../product.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { Product } from '../product';
import { CategoryService } from '../../category/category.service';
import { Category } from '../../category/category';

const DateIsFutureValidator: ValidatorFn = (control): ValidationErrors | null => {
  const now = (new Date()).getDay();
  const value = control.value?.getDay();

  const validatorComply = value && value > now;

  return validatorComply
    ? null
    : { dateIsFuture: { valid: false, value: control.value } };
};

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductFormComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();

  public productForm = new FormGroup({
    title: new FormControl(
      null,
      [Validators.required, Validators.minLength(5), Validators.maxLength(40)]
    ),
    category: new FormControl(null, Validators.required),
    price: new FormControl(null, [Validators.required, Validators.min(0)]),
    expirationDate: new FormControl(null, [Validators.required, DateIsFutureValidator]),
  });

  public isSubmitDisabled$ = this.productForm.statusChanges
    .pipe(
      startWith(this.productForm.status),
      map(status => status === 'INVALID'),
    );

  public strategy: 'edit' | 'add' = 'add';
  public productId: string;
  public categoryId: string;
  public product$: Observable<Product>;
  public categoryList$: Observable<Array<Category>> = this.categoryService.entityList;

  constructor(
    private readonly productService: ProductService,
    private readonly categoryService: CategoryService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
  ) {
  }

  ngOnInit(): void {
    this.strategy = this.activatedRoute.snapshot.data.strategy;
    this.productId = this.activatedRoute.snapshot.params.product;
    this.categoryId = this.activatedRoute.snapshot.params.category;

    if (this.strategy === 'edit' && this.productId) {
      this.initEditPrefill();
    }

    if (this.strategy === 'add' && this.categoryId) {
      this.initAddPrefill();
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }

  public save(): void {
    if (this.strategy === 'edit') {
      this.productService.update({
        ...this.productForm.value,
        id: this.productId,
      });
    } else {
      this.productService.add(this.productForm.value);
    }

    if (this.categoryId) {
      this.router.navigate(['/categories', this.categoryId, 'products']);
    } else {
      this.router.navigate(['/products']);
    }
  }

  private initEditPrefill(): void {
    this.product$ = this.productService.getEntity(this.productId).pipe(shareReplay(1));

    this.product$
      .pipe(
        take(1),
        takeUntil(this.destroy$),
      )
      .subscribe(
        product => {
          const {id, ...formPrefill} = product;

          this.productForm.patchValue(formPrefill);
        }
      );
  }

  private initAddPrefill(): void {
    this.categoryService.getEntity(this.categoryId)
      .pipe(
        take(1),
        takeUntil(this.destroy$),
      )
      .subscribe(
        ({ id }) => this.productForm.patchValue({ category: id })
      );
  }
}
