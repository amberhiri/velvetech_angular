import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Category } from '../../category/category';
import { ProductService } from '../product.service';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { Product } from '../product';
import { CategoryService } from '../../category/category.service';
import { map, tap, withLatestFrom } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { PageEvent } from '@angular/material/paginator';

export type ProductView = Omit<Product, 'category'> & { category: Category; };

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductListComponent implements OnInit {
  public displayedColumns: string[];
  public dataSource$: Observable<Array<Product | ProductView>>;
  public allProductsSource$: Observable<Array<Product | ProductView>>;
  public totalPages$: Observable<number>;
  public currentPage$ = new BehaviorSubject<number>(0);
  public perPage$ = new BehaviorSubject<number>(10);

  public category$: Observable<Category> = this.categoryService
    .getEntity(this.activatedRoute.snapshot.params.category);

  private allProducts$: Observable<Array<ProductView>> = this.productService.entityList
    .pipe(
      withLatestFrom(this.categoryService.entityList),
      map(([productList, categoryList]) => {
        return productList
          .map(product => ({
            ...product,
            category: categoryList.find(category => category.id === product.category),
          }));
      }),
    );

  private filteredByCategoryProducts$: Observable<Array<Product>> = this.productService.entityList
    .pipe(
      withLatestFrom(this.categoryService.getEntity(this.activatedRoute.snapshot.params.category)),
      map(([productList, category]) => {
        return productList
          .filter(product => product.category === category.id);
      }),
    );

  constructor(
    private readonly productService: ProductService,
    private readonly categoryService: CategoryService,
    private readonly activatedRoute: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    const category = this.activatedRoute.snapshot.params.category;
    this.displayedColumns = category
      ? [
        'title',
        'price',
        'expirationDate',
        'actions'
      ]
      : [
        'title',
        'category',
        'price',
        'expirationDate',
        'actions'
      ];

    this.allProductsSource$ = category
      ? this.filteredByCategoryProducts$
      : this.allProducts$;

    this.totalPages$ = this.allProductsSource$.pipe(map(list => list.length));

    this.dataSource$ = combineLatest([
      this.allProductsSource$,
      this.perPage$,
      this.currentPage$,
    ])
      .pipe(
        map(([products, perPage, currentPage]) => {
          const results = products.reduce((resultArray, item, index) => {
            const chunkIndex = Math.floor(index / perPage);

            if (!resultArray[chunkIndex]) {
              resultArray[chunkIndex] = [];
            }

            resultArray[chunkIndex].push(item);

            return resultArray;
          }, []);

          return results[currentPage] || results[0];
        }),
      );
  }

  public handlePageChanges(event: PageEvent): void {
    if (event.pageSize === this.perPage$.value) {
      this.currentPage$.next(event.pageIndex);
    } else {
      this.currentPage$.next(0);
      this.perPage$.next(event.pageSize);
    }
  }

  public remove(product: ProductView): void {
    this.productService.remove(product.id);
  }
}
