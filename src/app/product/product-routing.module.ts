import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductFormComponent } from './product-form/product-form.component';

const routes: Routes = [
  {
    path: '',
    component: ProductListComponent,
  },
  {
    path: ':product/details',
    component: ProductDetailComponent,
  },
  {
    path: 'edit/:product',
    component: ProductFormComponent,
    data : { strategy : 'edit' }
  },
  {
    path: 'add',
    component: ProductFormComponent,
    data : { strategy : 'add' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
