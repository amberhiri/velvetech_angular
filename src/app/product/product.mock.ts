import { Product } from './product';
import * as faker from 'faker';
import { categoryList } from '../category/category.mock';
import { CreateEntityFn } from '../base-crud-service';

export function getRandomIntInclusive(min: number, max: number): number {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export const createProduct: CreateEntityFn<Product> = (payload?: Partial<Product>): Product => {
  const category = categoryList[getRandomIntInclusive(0, categoryList.length - 1)];

  return {
    title: payload?.title || faker.commerce.productName(),
    id: faker.random.alphaNumeric(43),
    category: payload?.category || category.id,
    expirationDate: payload?.expirationDate || faker.date.recent(50),
    price: payload?.price || faker.finance.amount(100, 99999),
  };
};

export const productList: Array<Product> = new Array(200)
  .fill(1)
  .map(() => createProduct());
