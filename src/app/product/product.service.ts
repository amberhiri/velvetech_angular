import { Injectable } from '@angular/core';
import { productList, createProduct } from './product.mock';
import { Product } from './product';
import { BaseEntityService } from '../base-crud-service';

@Injectable({ providedIn: 'root' })
export class ProductService extends BaseEntityService<Product> {
  constructor() {
    super(productList, 'id', createProduct);
  }
}
